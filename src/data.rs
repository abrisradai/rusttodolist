#[derive(Debug, PartialEq)]
pub struct Data {
    subject: String,
    done: bool,
    deleted: bool,
}

impl Data {
    pub fn new(subject: String) -> Data {
        Data {
            subject,
            done: false,
            deleted: false,
        }
    }

    pub fn update(&mut self, new_subject: String) {
        self.subject = new_subject
    }

    pub fn done(&mut self) {
        self.done = true
    }

    pub fn is_done(&self) -> bool {
        self.done
    }

    pub fn delete(&mut self) {
        self.deleted = true
    }

    pub fn is_deleted(&self) -> bool {
        self.deleted
    }

    pub fn subject(& self) -> & String {
        &self.subject
    }
}


#[cfg(test)]
mod tests {
use super::*;

    #[test]
    fn subject_after_create() {
        let data = Data::new("Mamma mia!".to_string());
        assert_eq!(data.subject(), &"Mamma mia!".to_string())
    }

    #[test]
    fn subject_after_update() {
        let mut data = Data::new("Mamma mia!".to_string());
        data.update("The godfather".to_string());
        assert_ne!(data.subject(), &"Mamma mia!".to_string());
        assert_eq!(*data.subject(),"The godfather".to_string())
    }


    #[test]
    fn done_after_create() {
        let data = Data::new("Mamma mia!".to_string());
        assert!(!data.is_done())
    }

    #[test]
    fn done_after_update() {
        let mut data = Data::new("Mamma mia!".to_string());
        data.done();
        assert!(data.is_done())
    }

    #[test]
    fn delete_after_create() {
        let data = Data::new("Mamma mia!".to_string());
        assert!(!data.is_deleted())
    }

    #[test]
    fn delete_after_update() {
        let mut data = Data::new("Mamma mia!".to_string());
        data.delete();
        assert!(data.is_deleted())
    }
}
