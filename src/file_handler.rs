use crate::data::Data;
use std::fs;
use std::io::{self,Read,Write};
use std::path::Path;

pub struct FileHandler<T: AsRef<Path>>  {
    path: T,
}

impl<T: AsRef<Path>> FileHandler<T> {
    pub fn new(path: T) -> io::Result<Self> {
        fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(&path)?;

        Ok(Self{
            path,
        })
    }

    pub fn read(&self) -> io::Result<Vec<Data>> {

        let mut file = fs::File::open(&self.path)?;

        let mut contents = String::new();

        file.read_to_string(&mut contents)?;

        let datas = contents.lines()
            .map(|line| Data::new(line.to_string()))
            .collect();

        Ok(datas)
    }

    pub fn write(&self, datas: &[Data]) -> io::Result<()>{
        let mut file = fs::OpenOptions::new()
                        .write(true)
                        .truncate(true)
                        .open(&self.path)?;

        datas.iter()
            .filter(|data| !data.is_done())
            .map(|data| writeln!(file,"{}",data.subject()))
            .collect::<io::Result<Vec<()>>>()?;

        Ok(())

    }
}

#[cfg(test)]
mod tests{
    use super::*;

    fn setup<T: AsRef<Path>>(file: T) -> FileHandler<T>{
        let fh = match FileHandler::new(file){
            Ok(f) => f,
            Err(e) => panic!("file_handler_create_file FAILED with: {e}"),
        };
        fh
    }

    #[test]
    fn file_handler_create_file(){
        let file = "test.txt".to_string();
        let _ = setup(file.clone());
        assert!(std::path::Path::new(&file).exists())
    }

    #[test]
    fn read_from_empty_file(){
        let file = "empty.txt".to_string();
        let fh = setup(file);
        let empty = match fh.read(){
            Ok(f) => f,
            Err(e) => panic!("read_from_empty_file FAILED when read from file! With: {e}"),
        };
        assert!(empty.is_empty())
    }

    #[test]
    fn write_and_read_from_test_file(){
        let file = "test.txt".to_string();
        let fh = setup(file);
        let datas = vec![Data::new("test1".to_string()), Data::new("mit sütsz kis szűcs".to_string())];

        let _ = match fh.write(&datas){
            Ok(d) => d,
            Err(e) => panic!("write_and_read_from_test_filee FAILED when write into file! With: {e}"),
        };

        let read_datas = match fh.read(){
            Ok(d) => d,
            Err(e) => panic!("write_and_read_from_test_filee FAILED when read from file! With: {e}"),
        };

        assert_eq!(read_datas,datas);
    }

}