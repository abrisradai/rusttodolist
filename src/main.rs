use todo_list::cli::Cli;
use std::io;
fn main() -> io::Result<()> {

    let mut cli = Cli::try_default()?;
    cli.run()?;

    Ok(())
}
