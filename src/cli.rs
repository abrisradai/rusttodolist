use crate::data::Data;
use crate::file_handler::FileHandler;
use std::{io::{self, Write}, path::Path};

const FILE_NAME: &str = "todo.txt";
const WELCOME_MSG: &str = "
=============================================================
Welcome in the TODO LIST app, you have the following options:
=============================================================";
const OPTIONS: &str = "
-----------------------------
|1) add NEW element         |
|2) list existing elements  |
|3) modify element          |
|4) mark element as done    |
|5) delete element          |
|q) quit                    |
-----------------------------";

pub struct Cli<T: AsRef<Path>> {
    finished: bool,
    datas: Vec<Data>,
    file_handler: FileHandler<T>
}

impl Default for Cli<&str> {
    fn default() -> Self {
        Self::try_default().unwrap()
    }
}

impl Cli<&str> {
    pub fn try_default() -> io::Result<Self> {
        Self::new(FILE_NAME)
    }
}

impl<T: AsRef<Path>> Cli<T> {

    pub fn new(path: T) -> io::Result<Self> {
        let fl = FileHandler::new(path)?;
        let datas = fl.read()?;

        Ok(Self {
            finished: false,
            datas,
            file_handler: fl,
        })
    }

    pub fn run(&mut self) -> io::Result<()> {
        println!("{}", WELCOME_MSG);

        while !self.finished {
            println!("{}", OPTIONS);

            let mut read = String::new();
            io::stdin().read_line(&mut read)?;

            match read.trim() {
                "1" => self.add()?,
                "2" => self.list(),
                "3" => self.modify()?,
                "4" => self.done()?,
                "5" => self.delete()?,
                "q" => self.quit()?,
                x => println!(
                    "Your chosen character {x} is not an option, please choose from the following"
                ),
            }
        }
        Ok(())
    }

    fn add(&mut self) -> io::Result<()> {
        print!("Please add the new element: ");
        io::stdout().flush()?;

        let mut element = String::new();
        io::stdin().read_line(&mut element)?;

        self.datas.push(Data::new(element.trim().to_string()));
        println!("=================");
        println!("New element added");
        println!("=================");

        Ok(())
    }

    fn list(&self) {
        if self.datas.is_empty() {
            println!("=======================");
            println!("YOUR TODO LIST Is EMPTY");
            println!("=======================");
            return;
        }
        println!("==============");
        println!("YOUR TODO LIST");
        println!("==============");

        for (idx, data) in self.datas.iter().enumerate() {
            if data.is_done(){
                println!("[{}]: {} is DONE",idx + 1, data.subject());
            } else {
                println!("[{}]: {} is NOT DONE",idx + 1, data.subject());
            }
        }

        println!("==============");
    }

    fn modify(&mut self) -> io::Result<()> {
        println!("==============");
        let mut input = String::new();

        let idx = self.choosing("modify".to_string())?;

        println!("old value: {}", &self.datas[idx].subject());
        input.clear();
        print!("The new value: ");
        io::stdout().flush()?;

        io::stdin().read_line(&mut input)?;
        self.datas[idx].update(input.trim().to_string());
        println!("==============");
        println!("MODIFIED!");
        println!("==============");
        Ok(())
    }

    fn done(&mut self)  -> io::Result<()> {
        println!("==============");
        let idx = self.choosing("mark as done".to_string())?;

        self.datas[idx].done();

        println!("==============");
        println!("{} is DONE", &self.datas[idx].subject());
        println!("==============");
        Ok(())
    }

    fn delete(&mut self) -> io::Result<()> {
        println!("==============");
        let idx = self.choosing("delete".to_string())?;

        let subject = self.datas[idx].subject().clone();

        self.datas[idx].delete();

        self.datas.retain(|data| !data.is_deleted());

        println!("==============");
        println!("{} is DELETED", subject);
        println!("==============");
        Ok(())
    }

    fn quit(&mut self) -> io::Result<()> {
        self.file_handler.write(&self.datas)?;
        println!("==============");
        println!("{}", "Good bye!".to_uppercase());
        println!("==============");
        self.finished = true;
        Ok(())
    }

    fn choosing(&self, changing_text: String) -> io::Result<usize>{ //, io::Error
        let mut input = String::new();

        let mut idx = 0;
        while idx < 1 || idx > self.datas.len() {
            print!("Give the index of the element you would like to {}: ", changing_text);
            io::stdout().flush()?;

            input.clear();
            io::stdin().read_line(&mut input)?;
            idx = match input.trim().parse::<usize>(){
                Ok(v) => v,
                Err(e) => {
                    println!("Failed to parse {input} into 'usize': {e}");
                    println!("Your index must be between 1 and {}", self.datas.len());
                    continue;
                }
            };
        }

        let idx = idx - 1;
        Ok(idx)
    }
}
